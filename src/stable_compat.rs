use std::cmp::Ordering;
use std::slice::Iter;

pub trait SliceExt {
    fn is_sorted_ext(&self) -> bool;
}

impl<T: PartialOrd> SliceExt for [T] {
    fn is_sorted_ext(&self) -> bool {
        self.iter().is_sorted_by_ext(|a, b| a.partial_cmp(b))
    }
}

pub trait SliceIterExt: Iterator {
    fn is_sorted_by_ext<F>(self, compare: F) -> bool
        where
            Self: Sized,
            F: FnMut(&Self::Item, &Self::Item) -> Option<Ordering>;
}

impl<'a, T> SliceIterExt for Iter<'a, T> {
    fn is_sorted_by_ext<F>(self, mut compare: F) -> bool
        where
            Self: Sized,
            F: FnMut(&Self::Item, &Self::Item) -> Option<Ordering>
    {
        self.as_slice().windows(2).all(|w| {
            compare(&&w[0], &&w[1]).map(|o| o != Ordering::Greater).unwrap_or(false)
        })
    }
}